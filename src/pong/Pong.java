package pong;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.*;

public class Pong implements ActionListener, KeyListener{

	public static Pong pong;
	public int width = 550, height = 500;
	public Renderer renderer;

	public Paddle player1;
	public Paddle player2;
	
	public Ball ball;
	
	public boolean bot =  false, selectingDifficulty;
	
	public boolean w, s, up, down;
	
	public int gameStatus = 0, scoreLimit = 6, playerWon; // 0 = Menu, 1 = Paused, 2 = Playing, 3 = Game Over
	
	public int botDifficulty, botMoves, botCoolDown = 0;
	
	public Random random;
	
	public Pong(){
		Timer timer = new Timer(20, this);
		random = new Random();
		JFrame jframe1 = new JFrame("Pong");
		
		renderer = new Renderer();
		
		jframe1.setSize(width + 15, height + 30);
		jframe1.setVisible(true);
		jframe1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe1.add(renderer);
		jframe1.addKeyListener(this);
		
		timer.start();
	}
	
	public void start() {
		gameStatus = 2;
		player1 = new Paddle(this, 1);
		player2 = new Paddle(this, 2);
		ball = new Ball(this);
	}
	
	public void render(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		if (gameStatus == 0) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("arial", 10, 30));
			g.drawString("Pong", width / 2 - 30, 25);
			if (!selectingDifficulty) {
				g.setFont(new Font("arial", 10, 20));
				g.drawString("Press Space to play", width / 2 - 90, height / 2 - 25);
				g.drawString("Press Shift to play against CPU", width / 2 - 125, height / 2 + 25);
				g.drawString("<< Score Limit: " + scoreLimit + " >>", width / 2 - 75, height / 2 + 75);
			}
		}
		
		if (gameStatus == 1) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("arial", 10, 30));
			g.drawString("Paused", width / 2 - 53, height / 2 - 25);
		}
		
		if (selectingDifficulty) {
			String strDifficulty = botDifficulty == 0 ? "Easy": (botDifficulty == 1 ? "Medium" : "Hard");
			
			g.setFont(new Font("arial", 10, 20));
			g.drawString("<< Bot Difficulty: " + strDifficulty + " >>", width / 2 - 100, height / 2 - 25);
			g.drawString("Press Space to play", width / 2 - 75, height / 2 + 25);
		}
		
		if (gameStatus == 2 || gameStatus == 1) {
			g.setColor(Color.WHITE);
			g.setStroke(new BasicStroke(1F));
			g.drawLine(width / 2, 0, width / 2, height);
			g.setStroke(new BasicStroke(2F));
			g.drawOval(width / 2 - 75, height / 2 - 75, 150, 150);
			
			g.setFont(new Font("arial", 10, 30));
			g.drawString(String.valueOf(player1.score), width / 2 - 45, 25);
			g.drawString(String.valueOf(player2.score), width / 2 + 30, 25);
			
			player1.render(g);
			player2.render(g);
			ball.render(g);
		}

		if (gameStatus == 3) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("arial", 10, 30));
			g.drawString("Pong", width / 2 - 30, 25);

			g.drawString("Player " + playerWon + " wins!", width / 2 - 80, 60);

			g.setFont(new Font("arial", 10, 20));

			g.drawString("Press ESC to go back to the menu", width / 2 - 140, height / 2 + 25);
		}
	}
	
	public void update() {
		if (player1.score >= scoreLimit) {
			playerWon = 1;
			gameStatus = 3;
		}
		
		if (player2.score >= scoreLimit) {
			playerWon = 2;
			gameStatus = 3;
		}
		
		if (w) {
			player1.move(true);
		}
		if (s) {
			player1.move(false);
		}
		if (!bot) {
			if (up) {
				player2.move(true);
			}
			if (down) {
				player2.move(false);
			}
		}
		else {
			if (botCoolDown > 0) {
				botCoolDown--;
				
				if (botCoolDown == 0) {
					botMoves = 0;
				}
			}
			if (botMoves < 10) {
				if (player2.y + player2.height / 2 < ball.y) {
					player2.move(false);
					botMoves++;
				}
				if (player2.y + player2.height / 2 > ball.y) {
					player2.move(true);
					botMoves++;
				}
				if (botDifficulty == 0) {
					botCoolDown = 15;
				}
				if (botDifficulty == 1) {
					botCoolDown = 12;
				}
				if (botDifficulty == 2) {
					botCoolDown = 9;
				}

			}
		}
		
		ball.update(player1, player2);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (gameStatus == 2) {
			update();
		}
		
		renderer.repaint();		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int id = e.getKeyCode();
		
		if (id == KeyEvent.VK_W) {
			w = true;
		}
		
		else if (id == KeyEvent.VK_S) {
			s = true;
		}
		
		else if (id == KeyEvent.VK_UP) {
			up = true;
		}
		
		else if (id == KeyEvent.VK_DOWN) {
			down = true;
		}
		
		else if (id == KeyEvent.VK_RIGHT) {
			if (selectingDifficulty) {	
				if (botDifficulty < 2) {
					botDifficulty++;
				}
				else {
					botDifficulty = 0;
				}
			}
			else if (gameStatus == 0) {
				scoreLimit++;
			}
		}
		
		else if (id == KeyEvent.VK_LEFT) {
			if (selectingDifficulty) {
				if (botDifficulty > 0) {
					botDifficulty--;
				}
				else {
					botDifficulty = 2;
				}
			}
			else if (gameStatus == 0 && scoreLimit > 1) {
				scoreLimit--;
			}
		}
		
		else if (id == KeyEvent.VK_ESCAPE && (gameStatus == 2 || gameStatus == 3)) {
			gameStatus = 0;
		}
 		
		else if (id == KeyEvent.VK_SHIFT && (gameStatus == 0 || gameStatus == 3)) {
			bot = true;
			selectingDifficulty = true;
		}
		else if (id == KeyEvent.VK_SPACE) {
			if (gameStatus == 0) {
				if (!selectingDifficulty) {
					bot = false;
				}
				else {
					selectingDifficulty = false;
				}
				
				start();
			}
			else if (gameStatus == 1) {
				gameStatus = 2;
			}
			else if (gameStatus == 2) {
				gameStatus = 1;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int id = e.getKeyCode();
		
		if (id == KeyEvent.VK_W) {
			w = false;
		}
		
		else if (id == KeyEvent.VK_S) {
			s = false;
		}
		
		else if (id == KeyEvent.VK_UP) {
			up = false;
		}
		
		else if (id == KeyEvent.VK_DOWN) {
			down = false;
		}
	}
	
	public static void main(String[] args) {
		pong = new Pong();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
