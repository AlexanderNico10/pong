package pong;

import java.awt.Color;
import java.awt.Graphics;

public class Paddle {
	public int paddleNo;
	
	public int x, y, width = 30, height = 190;
	
	public int score;
	
	public Paddle(Pong pong, int paddleNo) {
		this.paddleNo = paddleNo;
		
		if (paddleNo == 1) {
			this.x = width - width;
		}
		else if (paddleNo == 2) {
			this.x = pong.width - width;		
		}

		this.y = pong.height / 2 - this.height / 2;
		
	}

	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(x, y, width, height);
		
	}

	public void move(boolean up) {
		int speed = 10;
		
		if (up) {
			if (y - speed > 0) {
				y-= speed;
			}
			else {
				y = 0;
			}
		}
		else {
			if (y + height + speed < Pong.pong.height) {
				y += speed;
			}
			else {
				y = Pong.pong.height - height;
			}
		}
	}
}