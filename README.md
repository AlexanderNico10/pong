Simple Pong
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/pong.git
---

File List
---

```
\src\pong

Ball.java
package-info.java
Paddle.java
Pong.java
Renderer.java
```

How to run application
---

Import the project onto Eclipse and run. Press the RIGHT arrow key to increase the
winning score, and LEFT to decrease. The SHIFT key begins a game against the CPU.
Select difficulty using the LEFT and RIGHT arrow keys, then press SPACE to begin.
The 'W' and 'S' keys are used as UP and DOWN controls for the leftmost paddle. The
SPACE bar pauses the game.

Alternatively, pressing the SPACE bar on startup will begin a two player game where
the rightmost paddle is controlled by the UP and DOWN arrow keys.

To end the game and return to the Main Menu, press ESC.